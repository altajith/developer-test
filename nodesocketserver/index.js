var app = require('express')();
var http = require('http').createServer(app);
var io = require('socket.io')(http);

app.get('/', function(req, res){
  res.send('Map server is online!');
});

io.on('connection', function(socket){
  socket.on('add-location', function(location){
    io.emit('broadcast', location);
  });
});

http.listen(3000, function(){
  console.log('listening on *:3000');
});