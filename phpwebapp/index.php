<?php
error_reporting(E_ALL);
ini_set('display_errors', 1);
date_default_timezone_set("Asia/Singapore");

define('BASE_PATH', __DIR__);

/**
 * Initiate the database
 *
 * @var string $host
 * @var string $username
 * @var string $password
 * @var string $db_name
 */
require(BASE_PATH.'/classes/Database.php');
$database = new \DB\Database('localhost', 'devtest', 'X463888oJ#', 'devtestdb');

/**
 * Function to load view with view name
 */
function load_view($view, $data) {
    $view_location = BASE_PATH.'/views/'.$view.'.php';
    if (file_exists($view_location)) {
        require_once($view_location);
    }
}

/**
 * Initiate controller classes
 */
$classes = [
    'MapController', 'LocationController'
];
foreach ($classes as $class) {
    $class_location = BASE_PATH.'/controllers/'.$class.'.php';
    if (file_exists($class_location)) {
        require_once($class_location);
    }
}

/**
 * Initiate routes for the php app
 */
$routes = [
    '/map' => 'MapController@index',
    '/locations' => 'LocationController@index',
    '/location/add' => 'LocationController@add'
];

//get the request uri to validate the route
$not_found = true;
$uri_parts = explode('?', $_SERVER['REQUEST_URI'], 2);
if (count($uri_parts) > 0) {
    $url_part = $uri_parts[0];
    //check url par in the routes array
    if (in_array($url_part, array_keys($routes))) {
        $not_found = false;
        if (array_key_exists($url_part, $routes)) {
            //separate controller and route init function
            $controller_parts = explode('@', $routes[$url_part]);
            if (count($controller_parts) == 2) { //This should only have two elements
                $controller_class = '\Controller\\'.$controller_parts[0];//setup the controller class with namespace
                $controller_function = $controller_parts[1];//controller function
                if (class_exists($controller_class) && method_exists($controller_class, $controller_function)) { //check if the class and the function is loaded to the php
                    $obj = new $controller_class();//create a object to the controller
                    $obj->$controller_function();//call the function for the route
                }
            }
        }
    }
}

/**Page not found response 404 */
if ($not_found) {
    header("HTTP/1.0 404 Not Found");
    echo "<h1>404 <small>Not Found</small></h1>";
    die();
}
?>