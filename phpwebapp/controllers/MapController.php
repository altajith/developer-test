<?php
namespace Controller;

class MapController {

    public function index() {
        global $database;
        $hours = 1;
        if (isset($_REQUEST['hours']) && $_REQUEST['hours'] > 1) {
            $hours = $_REQUEST['hours'];
        }
        $locations = $database->get_locations($hours);
        load_view('Map', ['locations' => $locations, 'hours' => $hours]);
    }

}