<?php
namespace Controller;

class LocationController {

    public function index() {
        load_view('Location', []);
    }

    public function add() {
        global $database;
        $database->add_location([
            'name' => $_POST['name'],
            'lat' => $_POST['lat'],
            'long' => $_POST['long'],
        ]);
        echo 'ok';
        exit;
    }
}