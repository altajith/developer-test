$(document).ready(function(){
    var is_map_available = false;
    if ($('#map').length > 0) {
        is_map_available = true;
        /**
         * Setup the mapbox
         */
        mapboxgl.accessToken = 'pk.eyJ1IjoiYWpwcmF6YSIsImEiOiJjanhlNGQ4MmcwNmtsNDBvNGQyYXV4eHhxIn0.CoZPv0qp_c53FfrE1yYOMA';
        var map = new mapboxgl.Map({
            container: 'map',
            style: 'mapbox://styles/mapbox/streets-v11',
            center: [103.865542, 1.327722], // starting position
            zoom: 4 // starting zoom
        });
        map.addControl(new mapboxgl.NavigationControl());
    }

    /**
     * Setup socket io
     */
    var socket = io('http://3.219.92.24:3000');
    socket.on('connect', function() {
        $('#connectionStatus').removeClass('red-box');
        $('#connectionStatus').addClass('green-box');
        $('#connectionStatus').text('Connected');
        if (is_map_available) {
            socket.on('broadcast', function(location) {
                var location_text = location.name+' @ '+location.lat+', '+location.long;
                var popup = new mapboxgl.Popup({ offset: 25 })
                            .setText(location_text);
                new mapboxgl.Marker()
                .setLngLat([location.long, location.lat])
                .setPopup(popup)
                .addTo(map);
                $('#locationListUl').append("<li>"+location_text+" added just now</li>");
            });
        }
    });
    socket.on('disconnect', function() {
        $('#connectionStatus').removeClass('green-box');
        $('#connectionStatus').addClass('red-box');
        $('#connectionStatus').text('Disconnected');
    });

    if ($('#addLocation').length > 0) {
        $('#addLocation').on("click", function(event) {
            var name = $('#name').val();
            var lat = $('#lat').val();
            var long = $('#long').val();
            if (name == '') {
                alert('Name cannot be empty!');
            } else if (lat == '' || lat == 0) {
                alert('Latitude cannot be empty or 0!');
            } else if (long == '' || long == 0) {
                alert('Longitude cannot be empty or 0!');
            } else if (!lat.match(/^-?\d*(\.\d+)?$/)) {
                alert('Latitude does not contain a valid input!');
            } else if (!long.match(/^-?\d*(\.\d+)?$/)) {
                alert('Longitude does not contain a valid input!');
            } else {
                $.ajax({
                    url: "/location/add",
                    type: "POST",
                    data: "name="+name+"&lat="+lat+"&long="+long,
                    success: function (response) {
                        if (response == 'ok') {
                            socket.emit('add-location', {
                                name: name,
                                lat: lat,
                                long: long
                            });
                            $('#name').val('');
                            $('#lat').val('');
                            $('#long').val('');
                            alert('Your location is successfully added!');
                        } else {
                            alert('Failed to add the location, please try again!');
                        }
                    },
                    error: function(jqXHR, textStatus, errorThrown) {
                       alert('Failed to add the location, please try again!');
                    }
                });
            }
        });
    }

    if (typeof locations !== undefined) {
        for (var i = 0; i < locations.length; i++) {
            var location = locations[i];
            var location_text = location.name+' : '+location.lat+', '+location.long+' on '+location.created_at;
            var popup = new mapboxgl.Popup({ offset: 25 })
                        .setText(location_text);
            new mapboxgl.Marker()
            .setLngLat([location.long, location.lat])
            .setPopup(popup)
            .addTo(map);
            $('#locationListUl').append("<li>"+location_text+"</li>");
        }
    }
});
