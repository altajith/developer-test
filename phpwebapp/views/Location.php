<!DOCTYPE html>
<html>
  <head>
    <title>Add Location</title>
    <meta name="viewport" content="initial-scale=1.0">
    <meta charset="utf-8">
    <link href='/css/style.css' rel='stylesheet' />
    <script src='/js/jquery-3.4.1.min.js'></script>
  </head>
  <body>

    <span id="connectionStatus"></span>

    <center>
      <div class="wrapper">
        <h1>Add Location</h1>
        <div class="input-wrapper">
          <label>Location Name</label>
          <input type="text" id="name" value="" />
        </div>
        <div class="input-wrapper">
          <label>Latitude</label>
          <input type="text" id="lat" value="" />
        </div>
        <div class="input-wrapper">
          <label>Longitude</label>
          <input type="text" id="long" value="" />
        </div>
        <button class="button" id="addLocation">Add +</button>
      </div>
    </center>

		<script src="/js/socket.io/socket.io.js"></script>
    <script src='/js/script.js'></script>
  </body>
</html>