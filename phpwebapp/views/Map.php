<!DOCTYPE html>
<html>
  <head>
    <title>Map View</title>
    <meta name="viewport" content="initial-scale=1.0">
    <meta charset="utf-8">
    <link href='/css/style.css' rel='stylesheet' />
    <script src='https://api.mapbox.com/mapbox-gl-js/v1.0.0/mapbox-gl.js'></script>
    <link href='https://api.mapbox.com/mapbox-gl-js/v1.0.0/mapbox-gl.css' rel='stylesheet' />
    <script src='/js/jquery-3.4.1.min.js'></script>
  </head>
  <body>
    <div class="floating-area">
      <span id="connectionStatus"></span>
      <div id="locationList">
        <form action="/map" method="get">
          <div class="input-wrapper">
            <label>Last Hours</label>
            <input type="number" class="half" name="hours" value="<?php echo $data['hours']; ?>" min="1" />
            <button type="submit" class="button">Search</button>
          </div>
        </form>
        <hr/>
        <ul id="locationListUl">
        </ul>
      </div>
    </div>
    <div id="map"></div>
    <script>
      var locations = <?php echo json_encode($data['locations']); ?>;
    </script>
		<script src="/js/socket.io/socket.io.js"></script>
    <script src='/js/script.js'></script>
  </body>
</html>