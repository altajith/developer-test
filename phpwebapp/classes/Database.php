<?php
namespace DB;

class Database {

    protected $host;

    protected $username;

    protected $password;

    protected $db_name;

    private $conn;

    public function __construct($host, $username, $password, $db_name) {
        $this->username = $username;
        $this->password = $password;
        $this->db_name = $db_name;
        try {
            //Establish the database connection
            $this->conn = new \PDO("mysql:host=$host;dbname=$db_name", $username, $password);
            $this->conn->setAttribute(\PDO::ATTR_ERRMODE, \PDO::ERRMODE_EXCEPTION);

            //Create locations table if it doesn't exists in the database
            $sql = "CREATE TABLE IF NOT EXISTS locations (
                        `id` INTEGER AUTO_INCREMENT PRIMARY KEY,
                        `name` VARCHAR(255) NOT NULL,
                        `lat` DOUBLE DEFAULT 0,
                        `long` DOUBLE DEFAULT 0,
                        `created_ip` VARCHAR(25) NOT NULL,
                        `created_at` DATETIME NOT NULL
                    );";
            $this->conn->exec($sql);
        } catch(\PDOException $e) {
            $this->conn = null;
            echo "Something wrong with the database connection.";
            exit;
        }
    }

    public function add_location($location) {
        $stmt = $this->conn->prepare("INSERT INTO locations (`name`, `lat`, `long`, `created_ip`, `created_at`)
                                    VALUES (:name, :lat, :long, :created_ip, :created_at)");
        $stmt->bindParam(':name', $location['name']);
        $stmt->bindParam(':lat', $location['lat']);
        $stmt->bindParam(':long', $location['long']);
        $stmt->bindParam(':created_ip', $_SERVER['REMOTE_ADDR']);
        $now = date('Y-m-d H:i:s');
        $stmt->bindParam(':created_at', $now);
        $stmt->execute();
    }

    public function get_locations($hours = 1) {
        $filter_date_time = date('Y-m-d H:i:s', strtotime("-$hours hours"));
        $stmt = $this->conn->prepare("SELECT * FROM locations WHERE created_at >= :fdt");
        $stmt->bindParam(':fdt', $filter_date_time);
        $stmt->execute();
        $locations = [];
        while ($row = $stmt->fetch(\PDO::FETCH_ASSOC)) {
            $row['created_at'] = date('Y-m-d h:i a', strtotime($row['created_at']));
            $locations[] = $row;
        }
        return $locations;
    }
}